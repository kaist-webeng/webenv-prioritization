# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import pandas as pd
from os.path import join
from collections import defaultdict
from approach_ml import calculate_apfd_cost
from approach_ml import ensure_dir
import time

def exact_matching(object_name, cache_range_threshold = 1, iter = 30, postfix=''):
    criteria_cols = {'backbone': ['browser_name', 'browser_version', 'os_info'],
                     'bootstrap': ['browser_name', 'browser_version', 'os_info'],
                     'lodash': ['browser_name', 'browser_version', 'build-type', 'module', 'os_info'],
                     'underscore': ['browser_name', 'browser_version', 'os_info']}
    target_cols = criteria_cols[object_name]

    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})

    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    # for debugging purpose
    df['cache_hit'] = np.nan # for debugging.

    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    apfd_dic = defaultdict(list)
    run_time_list = []
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue
        # print 'processing %d ...' %(build_id)

        test_df = df[df['build_id'] == build_id].copy()
        cache_range = (build_index - cache_range_threshold)
        cache_range = cache_range if cache_range >= 0 else 0
        cache_df = df[df['build_id'].isin(build_id_list[cache_range:build_index])]
        cache_df = cache_df[cache_df['passed'] == False].copy()
        test_df.reset_index(inplace=True)
        cache_df.reset_index(inplace=True)

        start_run_time = time.time()
        merged = pd.merge(test_df, cache_df, how='inner', left_on=target_cols, right_on=target_cols)
        matching_run_time = time.time() - start_run_time
        mask = test_df['index'].isin(merged['index_x'])
        test_df['cache_hit'] = mask

        # for debuggging
        test_df.set_index('index', inplace=True)
        df.loc[test_df.index, 'cache_hit'] = test_df['cache_hit']

        fault_info = test_df[test_df['passed'] == False]

        np.random.seed(1)
        schedule_run_time_list = []
        for i in range(iter):
            start_run_time = time.time()
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            scheduled_df = test_df.sort_values(by=['cache_hit', 'random'], ascending=[False, True])
            schedule_run_time = time.time() - start_run_time
            schedule_run_time_list.append(schedule_run_time)
            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)
        schedule_run_time_mean = np.array(schedule_run_time_list).mean()
        run_time_list.append([build_id, matching_run_time, schedule_run_time_mean])

    output_dir = join('..', 'output', object_name, 'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir, 'cache_exact%s.csv'%(postfix)))

    output_dir = join('..', 'output', object_name, 'criteria')
    ensure_dir(output_dir)
    df.to_csv(join(output_dir, '%s.csv' % ('cache_exact')))


def similarity_matching(object_name, cache_range_threshold = 1, iter=30, postfix=''):
    # 각 test record와 cache의 record의 jaccard similarity를 비교. 각 record의 평균 jaccard similarity를 기준으로 스케줄링
    criteria_cols = {'backbone': ['browser_name', 'browser_version', 'os_info'],
                     'bootstrap': ['browser_name', 'browser_version', 'os_info'],
                     'lodash': ['browser_name', 'browser_version', 'build-type', 'module', 'os_info'],
                     'underscore': ['browser_name', 'browser_version', 'os_info']}
    target_cols = criteria_cols[object_name]

    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})

    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    # for debugging purpose
    df['sim_score'] = np.nan # for debugging.

    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    apfd_dic = defaultdict(list)

    def jaccard_similarity(cache_record, test_record):
        set_test = set(test_record)
        set_cache = set(cache_record)
        denominator = set.union(set_test, set_cache)
        nominator = set.intersection(set_test, set_cache)
        jaccard_sim = len(nominator) / len(denominator)
        return jaccard_sim

    def jaccard_similarity_mean(test_record, cache):
        # print test_record
        return cache.apply(jaccard_similarity, axis=1, args=(test_record[target_cols],)).mean()

    run_time_list = []
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        test_df = df[df['build_id'] == build_id].copy()
        cache_range = (build_index - cache_range_threshold)
        cache_range = cache_range if cache_range >= 0 else 0
        cache_df = df[df['build_id'].isin(build_id_list[cache_range:build_index])]
        cache_df = cache_df[cache_df['passed'] == False].copy()

        start_run_time = time.time()
        test_df['sim_score'] = test_df.apply(jaccard_similarity_mean, axis=1, args=(cache_df[target_cols],))
        matching_run_time = time.time() - start_run_time

        # for debuggging
        df.loc[test_df.index, 'sim_score'] = test_df['sim_score']

        fault_info = test_df[test_df['passed'] == False]

        np.random.seed(1)
        schedule_run_time_list = []
        for i in range(iter):
            start_run_time = time.time()
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            scheduled_df = test_df.sort_values(by=['sim_score', 'random'], ascending=[False, True])
            schedule_run_time = time.time() - start_run_time
            schedule_run_time_list.append(schedule_run_time)
            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)
        schedule_run_time_mean = np.array(schedule_run_time_list).mean()
        run_time_list.append([build_id, matching_run_time, schedule_run_time_mean])

    output_dir = join('..', 'output', object_name, 'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir, 'cache_similarity%s.csv'%(postfix)))

    output_dir = join('..', 'output', object_name, 'criteria')
    ensure_dir(output_dir)
    df.to_csv(join(output_dir, '%s.csv' % ('cache_similarity')))


if __name__ == '__main__':
    object_names = ['backbone', 'underscore', 'lodash', 'bootstrap']

    for object_name in object_names:
        print object_name
        exact_matching(object_name)
        similarity_matching(object_name)

