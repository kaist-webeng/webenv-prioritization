from __future__ import division
import pandas as pd
import numpy as np
from os.path import join
from collections import defaultdict
from approach_ml import calculate_apfd_cost
from approach_ml import ensure_dir

def hybrid(object_name, first_method, second_method, iter=30):
    score_name_dic = {'BernoulliNB': ['will_be_failed'],
                  'cache_exact': ['cache_hit'],
                  'cache_similarity': ['sim_score'],
                  'failure_count': ['first_criteria', 'second_criteria']}

    df_1 = pd.read_csv(join('..', 'output', object_name, 'criteria', '%s.csv' % first_method), dtype={'browser_version': str})
    df_2 = pd.read_csv(join('..', 'output', object_name, 'criteria', '%s.csv' % second_method), dtype={'browser_version': str})

    columns_in_need_1 = ['build_id', 'passed', 'job_time', 'id'] + score_name_dic[first_method]
    df_1 = df_1[columns_in_need_1]
    columns_in_need_2 = ['id'] + score_name_dic[second_method]
    df_2 = df_2[columns_in_need_2]

    df = pd.merge(df_1, df_2, how='left', left_on='id', right_on='id')

    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    apfd_dic = defaultdict(list)
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue
        # print 'processing %d ...' % (build_id)

        test_df = df[df['build_id'] == build_id].copy()
        fault_info = test_df[test_df['passed'] == False]

        criteria = score_name_dic[first_method] + score_name_dic[second_method] + ['random']
        ascending_list =  [False]*(len(criteria)-1) + [True]

        np.random.seed(1)
        for i in range(iter):
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            scheduled_df = test_df.sort_values(by=criteria, ascending=ascending_list)
            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)

    output_dir = join('..','output',object_name,'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir, '%s+%s.csv'%(first_method, second_method)))

if __name__ == '__main__':
    first_methods = ['cache_exact', 'cache_similarity']
    second_methods = ['failure_count', 'BernoulliNB']
    object_names = ['backbone', 'underscore', 'lodash', 'bootstrap']
    for object_name in object_names:
        for first_method in first_methods:
            for second_method in second_methods:
                print object_name, first_method, second_method
                hybrid(object_name, first_method, second_method)