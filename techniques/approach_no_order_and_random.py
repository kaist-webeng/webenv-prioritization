import pandas as pd
from collections import defaultdict
import numpy as np
from approach_ml import calculate_apfd_cost
from os.path import join
from approach_ml import ensure_dir

def no_ordering(object_name):
    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})
    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    np.random.seed(1)

    apfd_dic = {}
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        print 'build_id: %d is tested...' % (build_id)

        test_df = df[df['build_id'] == build_id].copy()
        fault_info = test_df[test_df['passed'] == False]

        scheduled_df = test_df.sort_values(by=['id'], ascending=[True])
        scheduled_s = scheduled_df['job_time']
        apfd = calculate_apfd_cost(scheduled_s, fault_info)
        apfd_dic[build_id] = apfd

    output_dir = join('..','output',object_name,'apfd')
    ensure_dir(output_dir)
    pd.Series(apfd_dic).to_csv(join(output_dir,'no_ordering.csv'))

def random_ordering(object_name, iter=30):
    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})
    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()


    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    apfd_dic = defaultdict(list)
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        print 'build_id: %d is tested...' % (build_id)
        test_df = df[df['build_id'] == build_id].copy()
        fault_info = test_df[test_df['passed'] == False]

        np.random.seed(1)
        for i in range(iter):
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            scheduled_df = test_df.sort_values(by=['random'], ascending=[True])
            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)

    output_dir = join('..','output',object_name,'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir, 'random_ordering.csv'))


if __name__ == '__main__':
    object_names = ['backbone', 'underscore', 'lodash', 'bootstrap']

    for object_name in object_names:
        no_ordering(object_name)
        random_ordering(object_name)