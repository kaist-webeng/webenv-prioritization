# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
import pandas as pd
from os.path import join
from collections import defaultdict
from approach_ml import calculate_apfd_cost
from approach_ml import ensure_dir
import time


criteria_cols = {'backbone': ['browser_name', 'browser_version', 'os_info'],
                     'bootstrap': ['browser_name', 'browser_version', 'os_info'],
                     'lodash': ['browser_name', 'browser_version', 'build-type', 'module', 'os_info'],
                     'underscore': ['browser_name', 'browser_version', 'os_info']}

def fail_frequency(object_name, iter=30):
    # n개 다 일치하는 environment
    # 버전 빼고 다 일치하는 environment
    # ...

    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})
    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()
    target_cols = criteria_cols[object_name]

    # for debugging.
    df['first_criteria'] = np.nan
    df['second_criteria'] = np.nan

    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    apfd_dic = defaultdict(list)
    run_time_list = []
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        # 전체 cols 대상으로 exact matching - 1st criteria
        previous_failed_df = df[(df['build_id'] < build_id) & (df['passed'] == False)].copy()
        previous_failed_df['1st_count'] = 0
        previous_failed_df['2nd_count'] = 0

        ranking_1st = previous_failed_df[target_cols+['1st_count']].groupby(target_cols).count()\
                                                            .sort_values(by='1st_count', ascending=False)\
                                                            .reset_index()

        test_df = df.loc[df['build_id'] == build_id].copy()

        test_df.reset_index(inplace=True)

        start_run_time = time.time()
        merged = pd.merge(test_df, ranking_1st, how='inner', left_on=target_cols, right_on=target_cols)
        matching1_run_time = time.time() - start_run_time
        merged.set_index('index', inplace=True)

        test_df.set_index('index', inplace=True)
        test_df['first_criteria'] = 0
        test_df.loc[merged.index, 'first_criteria'] = merged['1st_count']

        # browser version col을 빼고 exact matching - 2nd criteria
        target_cols_reduced = target_cols[:]
        target_cols_reduced.remove('browser_version')
        ranking_2nd = previous_failed_df[target_cols_reduced + ['2nd_count']].groupby(target_cols_reduced).count() \
            .sort_values(by='2nd_count', ascending=False) \
            .reset_index()

        test_df.reset_index(inplace=True)
        start_run_time = time.time()
        merged = pd.merge(test_df, ranking_2nd, how='inner', left_on=target_cols_reduced, right_on=target_cols_reduced)
        matching2_run_time = time.time() - start_run_time
        merged.set_index('index', inplace=True)

        test_df.set_index('index', inplace=True)
        test_df['second_criteria'] = 0
        test_df.loc[merged.index, 'second_criteria'] = merged['2nd_count']

        # for debuggging
        df.loc[test_df.index, 'first_criteria'] = test_df['first_criteria']
        df.loc[test_df.index, 'second_criteria'] = test_df['second_criteria']

        fault_info = test_df[test_df['passed'] == False]

        np.random.seed(1)
        schedule_run_time_list = []
        for i in range(iter):
            start_run_time = time.time()
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            # scheduled_df = test_df.sort_values(by=['first_criteria','second_criteria', 'random'],
            #                                    ascending=[False, False, True])
            scheduled_df = test_df.sort_values(by=['first_criteria', 'random'],
                                               ascending=[False, True])
            schedule_run_time = time.time() - start_run_time
            schedule_run_time_list.append(schedule_run_time)
            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)
        schedule_run_time_mean = np.array(schedule_run_time_list).mean()
        run_time_list.append([build_id, matching1_run_time, matching2_run_time, schedule_run_time_mean])

    output_dir = join('..','output',object_name,'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir, 'failure_count.csv'))

    output_dir = join('..', 'output', object_name, 'criteria')
    ensure_dir(output_dir)
    df.to_csv(join(output_dir, '%s.csv' % ('failure_count')))

if __name__ == '__main__':
    object_names = ['backbone', 'underscore', 'lodash', 'bootstrap']

    for object_name in object_names:
        print object_name
        fail_frequency(object_name)

