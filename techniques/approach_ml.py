# -*- coding: utf-8 -*-
from __future__ import division
from os.path import join
import pandas as pd
import numpy as np
import time
from collections import defaultdict
import os

from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.linear_model import Perceptron
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelBinarizer
from sklearn.pipeline import FeatureUnion
from sklearn.externals import joblib

def ensure_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

def calculate_apfd_cost(scheduled_s, fault_info):
    # print 'number of failures:', len(fault_info)
    first = sum([scheduled_s.loc[i:].sum() for i in fault_info.index])
    second = scheduled_s[fault_info.index].sum() * -0.5
    third = scheduled_s.sum() * len(fault_info)
    # print first, second, third
    return (first+second)/third

class DataFrameSelector(BaseEstimator, TransformerMixin):
    def __init__(self, attribute_names):
        self.attribute_names = attribute_names
    def fit(self, X, y=None):
        return self
    def transform(self, X):
        return X[self.attribute_names].values

def training(object_name, ml_method, encoding_cols, postfix=''):
    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})
    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    transformer_list = []
    for encoding_col in encoding_cols:
        cat_pipeline = Pipeline([
            ('selector', DataFrameSelector(encoding_col)),
            ('label_binarizer', LabelBinarizer()),
        ])
        transformer_list.append((encoding_col, cat_pipeline))

    full_pipeline = FeatureUnion(transformer_list=transformer_list)

    ensure_dir("../output/%s/pickles" % object_name)

    run_time_list = []
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        # print 'build_id: %d is trained...'%(build_id)

        if ml_method == 'MultinomialNB':
            clf = MultinomialNB()
        elif ml_method == 'BernoulliNB':
            clf = BernoulliNB()
        elif ml_method == 'Perceptron':
            clf = Perceptron()
        elif ml_method == 'SGD':
            clf = SGDClassifier()
        elif ml_method == 'PAC':
            clf = PassiveAggressiveClassifier()

        # find the latest previous build that has new_combi
        # new_combi_build = latest_build_with_new_combi(df, build_id)
        new_combi_build = df.loc[(df['build_id'] < build_id) & (df['new_combi'] == True), 'build_id'].max()

        # 두번째 build는 아직 모델이 없으니 반드시 전체 training을 하도록.
        if build_index == 1:
            new_combi_build = build_id_list[0]

        right_prev_build_id = build_id_list[build_index-1]
        if (build_index == 1) or \
                (all(df.loc[df['build_id'] == right_prev_build_id, 'new_combi']) == False):
            build_again_flag = True
        else:
            build_again_flag = False

        build_again_start_time = time.time()

        # build clf until new_combi_build
        # 새로운 정보가 나오면 다시 training을 해야하니까.
        training_df = df[df['build_id'] <= new_combi_build]
        training_pass_labels = training_df['passed'].copy()
        training_df_prepared = full_pipeline.fit_transform(training_df)
        clf.partial_fit(training_df_prepared, training_pass_labels, [False, True])
        # increment the build with the rest builds

        if build_again_flag == True:
            build_again_total_time = time.time() - build_again_start_time
        else:
            build_again_total_time = 0

        # 새로운 정보가 있는 build 이후부터 현재 빌드 직전까지는 incremetnal learning
        prev_b_ids = [b for b in build_id_list if (b > new_combi_build) and (b < build_id)]
        partial_build_total_time = 0
        for prev_b_id in prev_b_ids:

            if prev_b_id == right_prev_build_id:
                partial_build_start_time = time.time()
            training_df = df[df['build_id'] == prev_b_id]
            training_pass_labels = training_df['passed'].copy()
            training_df_prepared = full_pipeline.transform(training_df)
            clf.partial_fit(training_df_prepared, training_pass_labels, [False, True])
            if prev_b_id == right_prev_build_id:
                partial_build_total_time =  time.time() - partial_build_start_time

        run_time_list.append([build_id, build_again_total_time, partial_build_total_time])

        joblib.dump(full_pipeline, "../output/%s/pickles/pipeline_%d%s.pkl"%(object_name, build_id, postfix))
        joblib.dump(clf, "../output/%s/pickles/clf_%s_%d%s.pkl"%(object_name, ml_method, build_id, postfix))


def testing(object_name, ml_method, iter=30, postfix=''):
    df = pd.read_table('../dataset/%s/testdata-all.tsv' % object_name, dtype={'browser_version': str})
    build_id_list = df.loc[df['passed'] == False, 'build_id'].sort_values().unique()

    # for debugging.
    df['will_be_failed'] = np.nan

    if object_name == 'lodash':
        # APFD. Not APFD_c
        df['job_time'] = 1

    apfd_dic = defaultdict(list)
    run_time_list = []
    for build_index, build_id in enumerate(build_id_list):
        if build_index == 0:
            continue

        # print 'build_id: %d is tested...'%(build_id)
        full_pipeline = joblib.load("../output/%s/pickles/pipeline_%d%s.pkl"%(object_name, build_id, postfix))
        clf = joblib.load("../output/%s/pickles/clf_%s_%d%s.pkl"%(object_name, ml_method, build_id, postfix))


        test_df = df[df['build_id'] == build_id].copy()
        start_run_time = time.time()
        test_df_prepared = full_pipeline.transform(test_df)

        if ml_method in ['MultinomialNB', 'BernoulliNB']:
            y_pred = clf.predict_log_proba(test_df_prepared)
            test_df['will_be_failed'] = pd.Series(y_pred[:, 0], index=test_df.index)
        else:
            # decision_function값이 높을 수록 pass가 될 가능성이 높다. 따라서 -를 해서 내림차순 정렬.
            y_pred = clf.decision_function(test_df_prepared)
            test_df['will_be_failed'] = -1 * y_pred

        preprocessing_time = time.time() - start_run_time

        # for debuggging
        df.loc[test_df.index, 'will_be_failed'] = test_df['will_be_failed']

        fault_info = test_df[test_df['passed'] == False]

        np.random.seed(1)
        schedule_run_time_list = []
        for i in range(iter):
            start_run_time = time.time()
            # new combi이면 전부 0으로 encording하는 듯
            # if len(test_df[test_df['new_combi']==True]) > 0:
            #    print build_id
            random_nums = np.arange(len(test_df))
            np.random.shuffle(random_nums)
            test_df['random'] = random_nums

            # test_pass_labels = test_df['passed'].copy() # APFD 구하기 떄문에 test y 값 필요없음
            scheduled_df = test_df.sort_values(by=['will_be_failed', 'random'], ascending=[False, True])
            schedule_run_time = time.time() - start_run_time
            schedule_run_time_list.append(schedule_run_time)

            scheduled_s = scheduled_df['job_time']
            apfd = calculate_apfd_cost(scheduled_s, fault_info)
            apfd_dic[build_id].append(apfd)

        schedule_run_time_mean = np.array(schedule_run_time_list).mean()
        run_time_list.append([build_id, preprocessing_time, schedule_run_time_mean])


    output_dir = join('..','output',object_name,'apfd')
    ensure_dir(output_dir)
    pd.DataFrame(apfd_dic).mean().to_csv(join(output_dir,'%s%s.csv'%(ml_method, postfix)))

    output_dir = join('..', 'output', object_name, 'criteria')
    ensure_dir(output_dir)
    df.to_csv(join(output_dir, '%s.csv' % (ml_method)))

if __name__ == '__main__':

    encoding_cols_dic = {
        'backbone': ['os_info', 'browser_name', 'browser_version'],
        'bootstrap': ['os_info', 'browser_name', 'browser_version'],
        'lodash': ['module', 'build-type', 'os_info', 'browser_name', 'browser_version'],
        'mootools': ['os_info', 'browser_name', 'browser_version'],
        'underscore': ['os_info', 'browser_name', 'browser_version']
    }

    object_names = ['backbone', 'underscore', 'lodash', 'bootstrap']
    ml_methods = ['BernoulliNB']

    for object_name in object_names:
        for ml_method in ml_methods:
            print ('%s with %s ...'%(object_name, ml_method))
            training(object_name, ml_method, encoding_cols_dic[object_name])
            testing(object_name, ml_method)
