# Dataset
* Dataset path for each object: `dataset/{object name}/testdata-all.tsv`

## Description of columns in dataset
* browser_name: Browser name
* browser_version: Browser version
* build_id: Build id
* creation_time: Creation time of a job
* error: Whether the build has error (True/False)
* job_time: Job duration time
* num_failed: the number of failed test cases
* os_info: Operating System information
* start_time: Start time of a job
* test_time: Duration time of running test cases
* passed: Whether tests has passed or not (True/False)
* id: Unique ID of a test environment
* new_combi: Whether a test environment is new or not (True/False)
* module: (Only Lodash) module name
* buid-type: (Only Lodash) build type

# Techniques
* Implementation is in `techniques` directory

# Virtual Environment
* This project is using Anaconda virtual environment

```
$ conda env export -p ~/anaconda/envs/webenv_prio > env.yml
$ conda env create -f env.yml
```

# How to run
1. cd techniques
2. python approach_*.py

